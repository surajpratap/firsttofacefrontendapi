window.firstToFace = angular.module 'firstToFace', [
  'ngRoute'
  'ui.bootstrap'
  'cgNotify'
  'angular-loading-bar'
  'angularFileUpload'
]

#HeaderController controls <head> tag
#most importantly the title
#it watches the title service for any change in title
#firstToFace.controller 'HeaderController', ['Title', '$scope', '$timeout', (Title, $scope, $timeout)->

#  @title = Title.title
#
#  #Any change in Title service.title should be reflected
#  #HeaderController watches the value of Title.title
#  $scope.$watch(
#    ->
#      return Title.title
#    (newValue, oldValue)=>
#      @title = newValue
#  )
#
#  return undefined
#]
#HeaderController Ends

#MainController holds <body>
firstToFace.controller 'MainController', ['CurrentUser', '$scope', 'notify', '$location', 'Backend', (CurrentUser, $scope, notify, $location, Backend)->

  @currentUser = CurrentUser.currentUser

  $scope.$watch(
    ->
      CurrentUser.currentUser
    (newValue, oldValue)=>
      @currentUser = newValue
  )

  @signOut = ->
    CurrentUser.logout().then(
      ->
        notify(
          message: 'Successfully signed out.'
          classes: ['alert-success']
        )
        $location.path '/'
      ->
        #THIS should not happen, in this case we sign out from frontend but backend is still signed in.
        #A new session session on backend with destroy this session though
        notify(
          message: 'Signed out but something went wrong.'
          classes: ['alert-danger']
        )
        $location.path '/'
    )

  @getSearchResults = (query)->
    Backend.post('/search/search_by_username', {query: query}).then(
      (response)->
        return response.data
    )

  @usernameSelected = (selectedUsername)->
    @selectedUsername = selectedUsername.username
    if selectedUsername and selectedUsername.username
      $location.path "/users/#{selectedUsername.username}"

  return undefined
]
#Main Controller Ends

# Service - Title
#this service is used to change title on each request
# Title.title, returns the title for current url
#firstToFace.factory 'Title', [->
#
#  service = {}
#
#  service.title = 'FirstToFace'
#
#  return service
#
#]

