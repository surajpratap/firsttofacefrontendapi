firstToFace.controller 'QuoteController', ['Backend', 'CurrentUser', 'notify', '$routeParams', '$location', (Backend, CurrentUser, notify, $routeParams, $location)->

  unless CurrentUser.currentUser
    notify(
      message: 'You need to sign in first'
      classes: ['alert-danger']
    )
    $location.path '/users/sign_in'

  @quote = {}

  Backend.get("/quotes/#{$routeParams.slug}").success(
    (data)=>
      @quote = data.quote
  ).error(
    ->
      notify(
        message: 'error fetching quote'
        classes: ['alert-danger']
      )
  )

  @newQuoteAnswer = {}

  @createNewQuoteAnswer = ->
    @newQuoteAnswer.quote_id = @quote.id
    Backend.post('/quote_answers', @newQuoteAnswer).success(
      (data)=>
        #add the returned (newly created quote answers to the array of quote_answers)
        @quote.quote_answers ||= []
        @quote.quote_answers.unshift(data.quote_answer)
        #and empty the newQuoteAnswer object
        @newQuoteAnswer = {}
        #notify
        notify(
          message: 'Added new re-quote'
          classes: ['alert-success']
        )
    ).error(
      (data)->
        if data and data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
        else
          notify(
            message: 'Could not add this answer.Try again.'
            classes: ['alert-danger']
          )
    )


  return undefined
]