#Backend Service
#This service wraps $http service of angular
#Use this all over the project to connect with backend
firstToFace.factory 'Backend', ['$http', '$window', '$upload', ($http, $window, $upload)->

  BACKEND_URL = 'https://surajpratap.herokuapp.com'

  service = {}

  service.sendFileWithData = (url, files, data)->
    $upload.upload({
      url: "#{BACKEND_URL}#{url}"
      fields: data
      file: files
      fileFormDataName: 'file[]'
      headers: {
        TOKEN:$window.localStorage['token']
      }
    })

  service.sendFile = (method, url, file)->
    $upload.upload({
      url: "#{BACKEND_URL}#{url}"
      file: file
      headers:{
        TOKEN: $window.localStorage['token']
      }
    })

  service.get = (url)->
    $http(
      url: "#{BACKEND_URL}#{url}.json"
      method: 'GET'
      headers: {
        TOKEN: $window.localStorage['token']
      }
    )

  service.post = (url, data)->
    $http(
      url: "#{BACKEND_URL}#{url}.json"
      method: 'POST'
      headers: {
        TOKEN: $window.localStorage['token']
      }
      data: data
    )

  service.put = (url, data)->
    $http(
      url: "#{BACKEND_URL}#{url}.json"
      method: 'PUT'
      headers: {
        TOKEN: $window.localStorage['token']
      }
      data: data
    )

  service.delete = (url)->
    $http(
      url: "#{BACKEND_URL}#{url}.json"
      method: 'DELETE'
      headers: {
        TOKEN: $window.localStorage['token']
      }
    )

  return service
]

#CurrentUser Service
#It records the token for current signed in user
#it also provides methods for sign_in and sign_out
firstToFace.factory 'CurrentUser', ['Backend', '$window', '$q', '$rootScope', (Backend, $window, $q, $rootScope)->

  service = {}

  service.currentUser = null

  #whenever localStorage changes, call CurrentUser to fetch it
  $rootScope.$watch(
    ->
      $window.localStorage['token']
    =>
      service.getCurrentUser().success(
        (data)=>
          if data.user
            service.currentUser = data.user
      ).error(
        =>
          service.currentUser = null
      )
  )

  service.login = (email, password)->
    #before this is done, explicitly remove token from localStorage
    #So the frontend is in conjugation with backend
    $window.localStorage['token'] = null

    request = Backend.post('/sessions', {email: email, password: password})
    #the controller should handle response
    request.success(
      (data)=>
        $window.localStorage['token'] = data.token
    )
    return request

  service.logout = ->
    request = Backend.delete('/sessions')
    $window.localStorage['token'] = null
    return request
  #the controller should handle response

  service.getCurrentUser = ->
    #this returns a promise for current user
    Backend.get('/users')

  #on initiation, call getCurrentUser
  #  this is not longer needed as the $watch is handling this
  #  service.getCurrentUser().then(
  #    #if successful,
  #    (data)=>
  #      service.currentUser = data
  #  )

  return service
]