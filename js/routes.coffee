#This is route configurations for firstToFace App
firstToFace.config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider)->

  $locationProvider.html5Mode(true)

  $routeProvider.when('/users/sign_in', {
    controller: 'SignInController'
    controllerAs: 'ctrl'
    templateUrl: '/templates/sign_in.html'
    title: 'FirstToFace - SignIn'
  }).when(
    '/users/sign_up',
    controller: 'SignUpController'
    controllerAs: 'ctrl'
    templateUrl: '/templates/sign_up/sign_up.html'
    title: 'FirstToFace - SignUp'
  ).when(
    '/',
    controller: 'IndexController'
    controllerAs: 'ctrl'
    templateUrl: '/templates/index.html'
    title: 'FirstToFace'
  ).when(
    '/users/:username',
    controller: 'ProfileController'
    controllerAs: 'ctrl'
  ).when(
    '/about',
    templateUrl: '/templates/static/about.html'
  ).when(
    '/contact',
    templateUrl: '/templates/static/contact.html'
    controller: 'ContactController'
    controllerAs: 'ctrl'
  ).when(
    '/t_and_c',
    templateUrl: '/templates/static/terms.html'
  ).when(
    '/privacy',
    templateUrl: '/templates/static/privacy.html'
  ).when(
    '/request_feature',
    templateUrl: '/templates/static/request_feature.html'
  ).when(
    '/questions/:slug',
    controller: 'QuestionController'
    controllerAs: 'ctrl'
    templateUrl: '/templates/question.html'
  ).when(
    '/quotes/:slug',
    controller: 'QuoteController'
    controllerAs: 'ctrl'
    templateUrl: '/templates/quote.html'
  ).when(
    '/step_2',
    controller: 'Step_2_Controller'
    controllerAs: 'ctrl'
    templateUrl: '/templates/sign_up/step_2.html'
  )

  return undefined
]
#config ends

firstToFace.run ['$rootScope', '$location', 'notify', 'CurrentUser', '$timeout', '$window', ($rootScope, $location, notify, CurrentUser, $timeout, $window)->

  unless $window.sessionStorage['beta-access'] and $window.sessionStorage['beta-access'] is 'suraj@firsttoface.com'
    password = prompt('Enter beta access password.')
    if password is 'suraj@firsttoface.com'
      $window.sessionStorage['beta-access'] = password
    else
      $location.path '/'
      $window.angular = null
      alert 'Unauthorised'


  $rootScope.$on('$routeChangeStart', (event, next, current)->
    path = null
    try
      path = next.originalPath

    #This piece of code determines the template to load on route /users/:username
    if path and path is '/users/:username'
      username = next.pathParams.username
      if CurrentUser.currentUser and CurrentUser.currentUser.username == username
        next.$$route.templateUrl = '/templates/current_profile.html'
      else
        next.$$route.templateUrl = '/templates/profile.html'
    #template for /users/:username ENDS
  )

  $rootScope.$on('$routeChangeError', (event, next, current)->
  )

  #On successful route change - all notices are removed
  #  $rootScope.$on('$routeChangeSuccess',
  #    ->
  #      notify.closeAll()
  #  )
  #commenting this for now, since it removes the notice before even showing once, maybe be timeout will work here

  $rootScope.$on('$routeChangeSuccess', (event, current, previous)->
    $timeout(
      ->
        if current and current.$$route and current.$$route.title
          $rootScope.title = current.$$route.title
        else
          $rootScope.title = 'FirstToFace'
      0
      false
    )
  )

  return undefined
]