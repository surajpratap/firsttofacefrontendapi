firstToFace.controller 'ProfileController', ['Backend', 'CurrentUser', 'notify', '$location', '$routeParams', '$rootScope', '$scope',(Backend, CurrentUser, notify, $location, $routeParams, $rootScope, $scope)->

  unless CurrentUser.currentUser
    notify(
      message: 'You have to sign in first.'
      classes: ['alert-danger']
    )
    $location.path '/users/sign_in'

  @user = {}

  Backend.get("/users/#{$routeParams.username}").success(
    (data)=>
      @user = data.user
      $rootScope.title = "FirstToFace - #{$routeParams.username}"

      #whenver profile page is rendered
      #check that the value of search bar should be equal to currentUsername
      if $scope.$parent.mainCtrl
        $scope.$parent.mainCtrl.selectedUsername = $routeParams.username
  ).error(
    ->
      notify(
        message: 'Something went wrong'
        classes: ['alert-danger']
      )
  )

  @uploadCoverImage = (files)->
    if files and files.length
      Backend.sendFile(
        'POST',
        '/users/cover_image',
        files[0]
      ).success(
        (data)=>
          if data.user and data.user.cover_image_url
            @user.cover_image_url = data.user.cover_image_url
      ).error(
        (data)->
          if data.errors and data.errors[0]
            notify(
              message: data.errors[0]
              classes: ['alert-danger']
            )
          else
            notify(
              message: 'Could not upload this image, try again.'
              classes: ['alert-danger']
            )
      )

  @uploadProfileImage = (files)->
    if files and files.length
      Backend.sendFile(
        'POST',
        '/users/profile_image',
        files[0]
      ).success(
        (data)=>
          if data.user and data.user.profile_image_url
            @user.profile_image_url = data.user.profile_image_url
            CurrentUser.currentUser.profile_image = data.user.profile_image_url.replace 'standard', 'thumb'
      ).error(
        (data)->
          if data.errors and data.errors[0]
            notify(
              message: data.errors[0]
              classes: ['alert-danger']
            )
          else
            notify(
              message: 'Could not upload this image, try again.'
              classes: ['alert-danger']
            )
      )

  @toggleFollowing = (user_id)->
    Backend.post('/followers/toggle', {user_id: user_id}).success(
      (data)=>
        @user.follow = data.follow
        currentUser = CurrentUser.currentUser
        if @user.follow
          @user.number_of_followers += 1
          @user.followers.unshift {
            id: currentUser.id
            name: currentUser.name
            username: currentUser.username
            profile_image: currentUser.profile_image
          }
        else
          @user.number_of_followers -= 1
          matchedIndex = null
          for object, index in @user.followers
            if object.id is currentUser.id
              matchedIndex = index
              break
          @user.followers.splice matchedIndex, 1
    ).error(
      (data)->
        if data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
        else
          notify(
            message: 'Something went wrong'
            classes: ['alert-danger']
          )
    )

  return undefined
]