#SIGNINCONTROLLER
firstToFace.controller 'SignInController', ['$location', 'notify', 'CurrentUser', ($location, notify, CurrentUser)->

#  CurrentUser.getCurrentUser().then(
#    (user)=>
#      @currentUser = user
#      notify(
#        message: 'You are already signed in.'
#        classes: ['alert-danger']
#      )
#      $location.path('/')
#  )

  if CurrentUser.currentUser
    notify(
      message: 'You are already signed in.'
      classes: ['alert-danger']
    )
    $location.path('/')

  #  Title.title = 'FirstToFace - SignIn'

  @email_ = null

  @password_ = null

  @signIn = ->
    CurrentUser.login(@email_, @password_).success(
      ->
        notify(
          message: 'Successfully signed in.'
          classes: ['alert-success']
        )
        $location.path('/')
    ).error(
      (data)->
        try
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
        catch
          notify(
            message: 'Invalid email or password'
            classes: ['alert-danger']
          )
    )

  return undefined
]
#SIGNIN CONTROLLER ENDS