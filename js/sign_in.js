// Generated by CoffeeScript 1.7.1
(function() {
  firstToFace.controller('SignInController', [
    '$location', 'notify', 'CurrentUser', function($location, notify, CurrentUser) {
      if (CurrentUser.currentUser) {
        notify({
          message: 'You are already signed in.',
          classes: ['alert-danger']
        });
        $location.path('/');
      }
      this.email_ = null;
      this.password_ = null;
      this.signIn = function() {
        return CurrentUser.login(this.email_, this.password_).success(function() {
          notify({
            message: 'Successfully signed in.',
            classes: ['alert-success']
          });
          return $location.path('/');
        }).error(function(data) {
          try {
            return notify({
              message: data.errors[0],
              classes: ['alert-danger']
            });
          } catch (_error) {
            return notify({
              message: 'Invalid email or password',
              classes: ['alert-danger']
            });
          }
        });
      };
      return void 0;
    }
  ]);

}).call(this);

//# sourceMappingURL=sign_in.map
