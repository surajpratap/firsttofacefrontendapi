firstToFace.controller 'Step_2_Controller', ['Backend', 'notify', 'CurrentUser', '$scope', '$location',(Backend, notify, CurrentUser, $scope, $location)->

  @currentUser = CurrentUser.currentUser

  $scope.$watch(
    ->
      CurrentUser.currentUser
    (newValue)=>
      @currentUser = newValue
  )

  @years = []

  @moreInfo = {}

  @months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

  year = (new Date()).getFullYear()

  for index in [0..140]
    @years.unshift year-index


  @uploadProfileImage = (images)->
    image = images[0]
    if image instanceof File
      Backend.sendFile('POST', '/users/profile_image', image).success(
        (data)=>
          if data and data.user and data.user.profile_image_url
            CurrentUser.currentUser.profile_image = data.user.profile_image_url
      ).error(
        ->
          notify(
            message: 'Something went wrong.Try again.'
            classes: ['alert-danger']
          )
      )

  @saveMoreInfo = ->
    gender = null
    date = null
    month = null
    year = null
    if @moreInfo.month and @moreInfo.date and @moreInfo.year
      month = @months.indexOf(@moreInfo.month)
      date = @moreInfo.date
      year = @moreInfo.year
    if @moreInfo.gender
      gender = @moreInfo.gender
    payload = {
      gender: gender
      date: date
      month: month
      year: year
    }
    Backend.put('/users', payload).success(
      ->
        $location.path '/'
    ).error(
      ->
        $location.path '/'
    )


  @skip = ->
    $location.path '/'

  return undefined
]