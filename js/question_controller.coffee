firstToFace.controller 'QuestionController', ['Backend', 'CurrentUser', '$location', '$routeParams', 'notify', (Backend, CurrentUser, $location, $routeParams, notify)->

  unless CurrentUser.currentUser
    notify(
      message: 'You need to sign in first.'
      classes: ['alert-danger']
    )
    $location.path '/users/sign_in'

  @question = null

  Backend.get("/questions/#{$routeParams.slug}").success(
    (data)=>
      @question = data.question
  ).error(
    ->
      notify(
        message: 'error fetching question'
        classes: ['alert-danger']
      )
  )

  @newQuestionAnswer = {}

  @createNewQuestionAnswer = ->
    @newQuestionAnswer.question_id = @question.id
    Backend.post('/question_answers', @newQuestionAnswer).success(
      (data)=>
        #add the returned (newly created question answers to the array of question_answers)
        @question.question_answers ||= []
        @question.question_answers.unshift(data.question_answer)
        #and empty the newQuestionAnswer object
        @newQuestionAnswer = {}
        #notify
        notify(
          message: 'Added new answer'
          classes: ['alert-success']
        )
    ).error(
      (data)->
        if data and data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
        else
          notify(
            message: 'Could not add this answer.Try again.'
            classes: ['alert-danger']
          )
    )

  return undefined
]