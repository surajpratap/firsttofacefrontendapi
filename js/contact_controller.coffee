firstToFace.controller 'ContactController', ['Backend', 'notify', (Backend, notify)->

  @newContact = {}

  @sendContactRequest = ->
    Backend.post('/contacts', @newContact).success(
      =>
        @newContact = {}

        notify(
          message: 'Your message is received. Thanks'
          classes: ['alert-success']
        )
        setTimeout(
          ->
            notify(
              message: 'We shall reply soon.'
            )
          2000
        )
    ).error(
      (data)->
        if data and data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
    )

  return undefined
]