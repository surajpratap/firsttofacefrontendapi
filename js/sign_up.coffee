#SignUpController
firstToFace.controller 'SignUpController', ['$location', 'notify', 'CurrentUser', 'Backend', '$timeout', ($location, notify, CurrentUser, Backend, $timeout)->


#  CurrentUser.getCurrentUser().then(
#    (user)=>
#      @currentUser = user
#      notify(
#        message: 'You are already signed in.'
#        classes: ['alert-danger']
#      )
#      $location.path('/')
#  )

#  Title.title = 'FirstToFace - SignUp'

  if CurrentUser.currentUser
    notify(
      message: 'You are already signed in.'
      classes: ['alert-danger']
    )
    $location.path('/')

  @newUser = {}

  @createNewUser = ->
    Backend.post('/users', @newUser).success(
      (user)=>
        notify(
          message: 'Successfully signed up.'
          classes: ['alert-success']
        )
        $timeout(
          ->
            notify(
              message: 'Signing you In.'
            )
        ,1000
        )
        $timeout(
          =>
            CurrentUser.login(@newUser.email, @newUser.password).success(
              (data)=>
                notify(
                  message: 'Signed in successfully.'
                  classes: ['alert-success']
                )
                $location.path('/step_2')
            ).error(
              ->
                notify(
                  message: 'Could not sign in.'
                  classes: ['alert-danger']
                )
            )
        ,2000
        )

    ).error(
      (data)->
        if data and data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
    )


  return undefined
]
#SignUpController ends