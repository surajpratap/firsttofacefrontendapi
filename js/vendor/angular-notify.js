//#angular-notify
//
//>A minimalistic (and extensible) notification service for Angular.
//
//                                                              [Live Demo](http://cgross.github.io/angular-notify/demo/)
//
//Supports IE 10, and recent versions of FF and Chrome.
//
//## Getting Started
//
//Install with Bower or download the the files directly from the dist folder in the repo.
//
//```bash
//bower install angular-notify --save
//```
//
//Add `dist/angular-notify.js` and `dist/angular-notify.css` to your index.html.
//
//    Add `cgNotify` as a module dependency for your module.
//
//```js
//angular.module('your_app', ['cgNotify']);
//```
//
//Then inject and use the `notify` service.
//
//```js
//function myController($scope,notify){  // <-- Inject notify
//
//    notify('Your notification message'); // <-- Call notify with your message
//
//    notify({ message:'My message', templateUrl:'my_template.html'} );
//
//}
//```
//
//## Options
//
//
//### notify(String|Object)
//
//The `notify` function can either be passed a string or an object.  When passing an object, the object parameters can be:
//
//    * `message` - Required.  The message to show.
//    * `templateUrl` - Optional.  A custom template for the UI of the message.
//    * `classes` - Optional. A list of custom CSS classes to apply to the message element.
//    * `messageTemplate` - Optional. A string containing any valid Angular HTML which will be shown instead of the regular `message` text. The string must contain one root element like all valid Angular HTML templates (so wrap everything in a `<span>`).
//* `scope` - Optional.  A valid Angular scope object.  The scope of the template will be created by calling `$new()` on this scope.
//* `position` - Optional.  Currently `center` and `right` are the only acceptable values.
//* `container` - Optional.  Element that contains each notification.  Defaults to `document.body`.
//
//This function will return an object with a `close()` method and a `message` property.
//
//### notify.config(Object)
//
//Call `config` to set the default configuration options for angular-notify.  The following options may be specified in the given object:
//
//* `duration` - The default duration (in milliseconds) of each message.  A duration of 0 will prevent messages from closing automatically.
//* `startTop` - The Y pixel value where messages will be shown.
//* `verticalSpacing` - The number of pixels that should be reserved between messages vertically.
//* `templateUrl` - The default message template.
//* `position` - The default position of each message.  Currently only `center` and `right` are the supported values.
//* `container` - The default element that contains each notification.  Defaults to `document.body`.
//
//### notify.closeAll()
//
//Closes all currently open notifications.
//
//## Providing Custom Templates
//
//Angular-notify comes with a very simplistic default notification template.  You are encouraged to create your own template and style it appropriate to your application.  Templates can also contain more advanced features like buttons or links.  The message templates are full Angular partials that have a scope (and a controller if you use `ng-controller="YourCtrl"`).
//
//The scope for the partial will either be descended from `$rootScope` or the scope specified in the `notify({...})` options.  The template scope will be augmented with a `$message` property, a `$classes` property, and a special `$close()` function that you may use to close the notification.
//
//The `messageTemplate` property is also included on the scope as `$messageTemplate`.  To ensure your custom template works with the `messageTemplate` option, your template should hide the normal text if `$messageTemplate` contains a value, and should have an element with the `cg-notify-message-template` class.  The element with the `cg-notify-message-template` class will have the compiled template appended to it automatically.
//
//


angular.module('cgNotify', []).factory('notify',['$timeout','$http','$compile','$templateCache','$rootScope',
    function($timeout,$http,$compile,$templateCache,$rootScope){

        var startTop = 10;
        var verticalSpacing = 15;
        var duration = 10000;
        var defaultTemplateUrl = 'angular-notify.html';
        var position = 'center';
        var container = document.body;

        var messageElements = [];

        var notify = function(args){

            if (typeof args !== 'object'){
                args = {message:args};
            }

            //THIS WILL CLOSE ALL OTHER NOTIFICATIONS
            //COMMENT THIS OUT TO REMOVE THIS BEHAVIOUR
            notify.closeAll();

            args.templateUrl = args.templateUrl ? args.templateUrl : defaultTemplateUrl;
            args.position = args.position ? args.position : position;
            args.container = args.container ? args.container : container;
            args.classes = args.classes ? args.classes : '';

            var scope = args.scope ? args.scope.$new() : $rootScope.$new();
            scope.$message = args.message;
            scope.$classes = args.classes;
            scope.$messageTemplate = args.messageTemplate;

            $http.get(args.templateUrl,{cache: $templateCache}).success(function(template){

                var templateElement = $compile(template)(scope);
                templateElement.bind('webkitTransitionEnd oTransitionEnd otransitionend transitionend msTransitionEnd', function(e){
                    if (e.propertyName === 'opacity' ||
                        (e.originalEvent && e.originalEvent.propertyName === 'opacity')){

                        templateElement.remove();
                        messageElements.splice(messageElements.indexOf(templateElement),1);
                        layoutMessages();
                    }
                });

                if (args.messageTemplate){
                    var messageTemplateElement;
                    for (var i = 0; i < templateElement.children().length; i ++){
                        if (angular.element(templateElement.children()[i]).hasClass('cg-notify-message-template')){
                            messageTemplateElement = angular.element(templateElement.children()[i]);
                            break;
                        }
                    }
                    if (messageTemplateElement){
                        messageTemplateElement.append($compile(args.messageTemplate)(scope));
                    } else {
                        throw new Error('cgNotify could not find the .cg-notify-message-template element in '+args.templateUrl+'.');
                    }
                }

                angular.element(args.container).append(templateElement);
                messageElements.push(templateElement);

                if (args.position === 'center'){
                    $timeout(function(){
                        templateElement.css('margin-left','-' + (templateElement[0].offsetWidth /2) + 'px');
                    });
                }

                scope.$close = function(){
                    templateElement.css('opacity',0).attr('data-closing','true');
                    layoutMessages();
                };

                var layoutMessages = function(){
                    var j = 0;
                    var currentY = startTop;
                    for(var i = messageElements.length - 1; i >= 0; i --){
                        var shadowHeight = 10;
                        var element = messageElements[i];
                        var height = element[0].offsetHeight;
                        var top = currentY + height + shadowHeight;
                        if (element.attr('data-closing')){
                            top += 20;
                        } else {
                            currentY += height + verticalSpacing;
                        }
                        element.css('top',top + 'px').css('margin-top','-' + (height+shadowHeight) + 'px').css('visibility','visible');
                        j ++;
                    }
                };

                $timeout(function(){
                    layoutMessages();
                });

                if (duration > 0){
                    $timeout(function(){
                        scope.$close();
                    },duration);
                }

            }).error(function(data){
                throw new Error('Template specified for cgNotify ('+args.templateUrl+') could not be loaded. ' + data);
            });

            var retVal = {};

            retVal.close = function(){
                if (scope.$close){
                    scope.$close();
                }
            };

            Object.defineProperty(retVal,'message',{
                get: function(){
                    return scope.$message;
                },
                set: function(val){
                    scope.$message = val;
                }
            });

            return retVal;

        };

        notify.config = function(args){
            startTop = !angular.isUndefined(args.startTop) ? args.startTop : startTop;
            verticalSpacing = !angular.isUndefined(args.verticalSpacing) ? args.verticalSpacing : verticalSpacing;
            duration = !angular.isUndefined(args.duration) ? args.duration : duration;
            defaultTemplateUrl = args.templateUrl ? args.templateUrl : defaultTemplateUrl;
            position = !angular.isUndefined(args.position) ? args.position : position;
            container = args.container ? args.container : container;
        };

        notify.closeAll = function(){
            for(var i = messageElements.length - 1; i >= 0; i --){
                var element = messageElements[i];
                element.css('opacity',0);
            }
        };

        return notify;
    }
]);

angular.module('cgNotify').run(['$templateCache', function($templateCache) {
    'use strict';

    $templateCache.put('angular-notify.html',
            "<div class=\"cg-notify-message\" ng-class=\"$classes\">\n" +
            "\n" +
            "    <div ng-show=\"!$messageTemplate\">\n" +
            "        {{$message}}\n" +
            "    </div>\n" +
            "\n" +
            "    <div ng-show=\"$messageTemplate\" class=\"cg-notify-message-template\">\n" +
            "        \n" +
            "    </div>\n" +
            "\n" +
            "    <button type=\"button\" class=\"cg-notify-close\" ng-click=\"$close()\">\n" +
            "        <span aria-hidden=\"true\">&times;</span>\n" +
            "        <span class=\"cg-notify-sr-only\">Close</span>\n" +
            "    </button>\n" +
            "\n" +
            "</div>"
    );

}]);