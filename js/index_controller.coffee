firstToFace.controller 'IndexController', ['CurrentUser', '$scope', 'Backend', 'notify', (CurrentUser, $scope, Backend, notify)->

  @currentUser = CurrentUser.currentUser

  $scope.$watch(
    ->
      CurrentUser.currentUser
    (newValue)=>
      @currentUser = newValue
      if @currentUser
        fetchQuestions()
        fetchQuotes()
        fetchFeed()
  )

  #TMP QUESTION IMAGES DATA STRUCTURE
  @tmpQuestionImages = []

  @updateTmpQuestionImagesDS = (images)->
    @newQuestion.images ||= []
    for image in images
      if image instanceof File
        if @newQuestion.images.length >= 5
          notify(
            message: '5 images are enough. :)'
          )
          break
        else
          @newQuestion.images.push image
          reader = new FileReader()
          reader.onload = (event)=>
            @tmpQuestionImages.push event.target.result
            $scope.$apply()
          reader.readAsDataURL(image)

  @removeFromTmpQuestionImagesDS = (index)->
    @newQuestion.images.splice index, 1
    @tmpQuestionImages.splice index, 1

  ##################################

  #TMP QUOTE IMAGES DATA STRUCTURE
  @tmpQuoteImages = []

  @updateTmpQuoteImagesDS = (images)->
    @newQuote.images ||= []
    for image in images
      if image instanceof File
        if @newQuote.images.length >= 5
          notify(
            message: '5 images are enough. :)'
          )
          break
        else
          @newQuote.images.push image
          reader = new FileReader()
          reader.onload = (event)=>
            @tmpQuoteImages.push event.target.result
            $scope.$apply()
          reader.readAsDataURL(image)

  @removeFromTmpQuoteImagesDS = (index)->
    @newQuote.images.splice index, 1
    @tmpQuoteImages.splice index, 1

  ###################################

  #if current user is present, feed part is shown
  @newQuestion = {}

  @createNewQuestion = ->
    images = @newQuestion.images
    delete @newQuestion.images
    Backend.sendFileWithData(
      '/questions',
      images,
      @newQuestion
    ).success(
      (data)=>
        notify(
          message: 'Question added!'
          classes: ['alert-success']
        )
        @newQuestion = {}
        @tmpQuestionImages = []
        @myQuestions ||= []
        @myQuestions.unshift(data.question)
    ).error(
      (data)=>
        @newQuestion.images = images
        if data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
        else
          notify(
            message: 'Could not add this question'
            classes: ['alert-danger']
          )
    )

  @myQuestions = []

  #fetch last 10 questions of current user
  #if current_user is present
#  if @currentUser
  fetchQuestions = =>
    Backend.get('/questions').success(
      (data)=>
        @myQuestions = data.questions
    )


  @newQuote = {}

  @myQuotes = []

  @createNewQuote = ->
    images = @newQuote.images
    delete @newQuote.images
    Backend.sendFileWithData(
      '/quotes',
      images,
      @newQuote
    ).success(
      (data)=>
        @newQuote = {}
        @tmpQuoteImages = []
        @myQuotes ||= []
        @myQuotes.unshift(data.quote)
        notify(
          message: 'Quote added!'
          classes: ['alert-success']
        )
    ).error(
      (data)=>
        @newQuote.images = images
        if data.errors and data.errors[0]
          notify(
            message: data.errors[0]
            classes: ['alert-danger']
          )
        else
          notify(
            message: 'Could not add this question'
            classes: ['alert-danger']
          )
    )

  fetchQuotes = =>
    Backend.get('/quotes').success(
      (data)=>
        @myQuotes = data.quotes
    )


  ###########
  #FEED PART
  @myFeed = []
  fetchFeed = =>
    Backend.get('/feeds').success(
      (data)=>
        if data.feed
          @myFeed = data.feed
    )

  return undefined
]